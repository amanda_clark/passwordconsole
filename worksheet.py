from hashlib import sha256
import mysql.connector
from pbkdf2_ctypes import *
import hmac
import hashlib
import binascii
from os import urandom
from cryptography.hazmat.primitives.ciphers import Cipher, algorithms, modes
from cryptography.hazmat.backends import default_backend
import getpass

class PasswordRow():
    def __init__(self, service, password, salt, iv):
        self.service = service
        self.password = password
        self.salt = salt
        self.iv = iv

def getAesHash(password, keyHash, iv):
    """
    AES 256 encryption of the password for the service.
    the slice makes sure the key is the appropriate length
    for AES-256
    """
    backend = default_backend()
    key = keyHash[0:32]  # Make sure key is appropriate length
    cipher = Cipher(algorithms.AES(key), modes.CFB(iv), backend=backend)
    encryptor = cipher.encryptor()
    ct = encryptor.update(password.encode('utf-8')) + encryptor.finalize()
    return ct


def insertPwd(master, service, password):
    """
    Creates a password for a given service to be encrypted
    Encrypts the service with HMAC, and the password with AES-256
    Probably should be refactored, because this function does too much
    Inserts encrypted data (with salt and iv, so the keys can be re derived
    from the master password) into pwds table
    """
    master = master.encode('utf-8')
    service = service.encode('utf-8')
    # Create HMAC for the service
    hashSvc = hmac.new(master, service, hashlib.sha224)
    salt = urandom(32)
    # Key to be used for AES
    keyHash = pbkdf2_hex(masterpassword.encode('utf-8'), salt)
    iv = urandom(16)
    aesHash = getAesHash(password, keyHash, iv)  # Returns the actual encrypted password
    aesHash = binascii.hexlify(aesHash)
    aesHash = aesHash.decode()
    salt = binascii.hexlify(salt)
    salt = salt.decode()
    iv = binascii.hexlify(iv)
    iv = iv.decode()
    hashSvc = hashSvc.hexdigest()
    cnx = mysql.connector.connect(user='', password='', host='127.0.0.1', database='passwords')
    # Encrypt the service, password, and the salt and iv. Salt is used for the pbkdf2 derivation, iv for the actual AES
    # encryption
    addMsg = ("INSERT INTO pwds "
              "(service, password, salt, iv) "
              "VALUES (%s, %s, %s, %s)")
    dataToAdd = (str(hashSvc), aesHash, salt, iv)
    cursor = cnx.cursor()
    cursor.execute(addMsg, dataToAdd)
    cnx.commit()
    cursor.close()
    cnx.close()


def getPwd(domain, masterpassword):
    """Retreives the password for a given domain name and
    the masterpassword
    """
    Passwords = {}
    cnx = mysql.connector.connect(user='', password='', host='127.0.0.1', database='passwords')
    query = ("SELECT * FROM pwds")
    cursor = cnx.cursor(dictionary=True)
    cursor.execute(query)
    for row in cursor:
        Passwords[row['service']] = row['password'] #Easy to use dict for service and passsword
        # newRow is a convenient class I created to store the data from a row from the password table
        #that holds all of the information needed to decrypt a password
        newRow = PasswordRow(row['service'], row['password'], row['salt'], row['iv'])
    domain = domain.encode('utf-8')
    masterpassword = masterpassword.encode('utf-8')
    hashSvc = hmac.new(masterpassword, domain, hashlib.sha224)
    hashSvc = hashSvc.hexdigest()
    if hashSvc in Passwords: #Checks to see if user supplied domain exists
        decrypted = decryptPwd(newRow, masterpassword) #If so, decrypt the password for the domain
        return decrypted
    else:
        return "Domain not found"


def decryptPwd(newRow, masterpassword):
    """Decrypts the password given the key, salt and iv
    from the row in the table that I used the class to conveniently store"""
    backend = default_backend()
    newRow.salt = newRow.salt.encode('utf-8')
    newRow.salt = binascii.unhexlify(newRow.salt)
    keyHash = pbkdf2_hex(masterpassword, newRow.salt)
    key = keyHash[0:32]  # Make sure key is appropriate length
    newRow.iv = newRow.iv.encode('utf-8')
    newRow.iv = binascii.unhexlify(newRow.iv)
    newRow.password = newRow.password.encode('utf-8')
    newRow.password = binascii.unhexlify(newRow.password)
    cipher = Cipher(algorithms.AES(key), modes.CFB(newRow.iv), backend=backend)
    decryptor = cipher.decryptor()
    newRow.password = decryptor.update(newRow.password) + decryptor.finalize()
    return newRow.password


def checkPwdTotal():
    """
    Simply checks how many passwords
    are stored in the master table.
    Its sorta an ugly hack for now
    :return:
    """
    cnx = mysql.connector.connect(user='', password='', host='127.0.0.1', database='passwords')
    sql = """SELECT COUNT(*) FROM master"""
    cursor = cnx.cursor()
    cursor.execute(sql)
    total = cursor.fetchone()
    if total != (0,):
        return 1
    else:
        return 0


def setMasterPwd(masterpassword):
    """
    stores the pbkdf2 hash of the master password, as well as the salt used
    so it can be later used to verify the user's entered master password
    """
    # The table should have only one master password stored
    # If there's already a master password, delete it
    tot = checkPwdTotal()
    if tot > 0:
        salt = urandom(16)
        masterpassword = pbkdf2_hex(masterpassword.encode('utf-8'), salt)
        password = masterpassword.decode()
        salt = binascii.hexlify(salt)
        salt = salt.decode()
        cnx = mysql.connector.connect(user='', password='', host='127.0.0.1', database='passwords')
        delete = """delete from master"""
        sql = """INSERT into master (masterhash, salt) values ('%s', '%s')""" % \
              (password, salt)
        cursor = cnx.cursor()
        cursor.execute(delete)
        cursor.execute(sql, password)
        cnx.commit()
        cursor.close()
        cnx.close()

    else:
        salt = urandom(16)
        masterpassword = pbkdf2_hex(masterpassword.encode('utf-8'), salt)
        password = masterpassword.decode()
        salt = binascii.hexlify(salt)
        salt = salt.decode()
        cnx = mysql.connector.connect(user='', password='', host='127.0.0.1', database='passwords')
        sql = """INSERT into master (masterhash, salt) values ('%s', '%s')""" % \
              (password, salt)
        cursor = cnx.cursor()
        cursor.execute(sql, password)
        cnx.commit()
        cursor.close()
        cnx.close()


def checkMaster(masterpassword):
    """Checks to see if the master password entered
    by user is the same as the master password pbkdf2 hash stored in the
    database. Retrieves the hash and the salt, and calculates hash of the
    supplied master password"""
    cnx = mysql.connector.connect(user='', password='', host='127.0.0.1', database='passwords')
    cursor = cnx.cursor()
    query = ("SELECT * FROM master")
    cursor.execute(query)
    for k, v in cursor:
        pwd = k
        salt = v
    salt = salt.encode('utf-8')
    salt = binascii.unhexlify(salt)
    newHash = pbkdf2_hex(masterpassword.encode('utf-8'), salt)
    newHash = newHash.decode()
    if pwd == newHash:
        return 1
    else:
        return 0


if __name__ == "__main__":
    ans = True
    while ans:
        print("==============================================================")
        print("* Welcome to the password manager, please enter your option  *")
        print("*                                                            *")
        print("*       1. Set master password                               *")
        print("*       2. Insert password for domain                        *")
        print("*       3. Retrieve password for domain                      *")
        print("*       4. Quit                                              *")
        print("*                                                            *")
        print("==============================================================")
        ans = input("What would you like to do? ")
        if ans == "1":
            masterpassword = getpass.getpass("Enter master password to set: ")
            setMasterPwd(masterpassword)
        elif ans == "2":
            masterpassword = getpass.getpass("Enter master password: ")
            domain = input('Enter domain: ')
            pwd = getpass.getpass("Enter password for domain: ")
            if pwd != 0:
                insertPwd(masterpassword, domain, pwd)
            else:
                print("Invalid master password")
        elif ans == "3":
            domain = input('Enter Domain to Get Password For: ')
            masterpassword = getpass.getpass("Enter master password: ")
            if checkMaster(masterpassword):
                resp = getPwd(domain, masterpassword)
                # print("You entered " + domain + " and it was found in the database")
                print("The password for "+domain+" is "+resp.decode())
                # else:
                #   print("Domain " + domain + " not found")
            else:
                print("Incorrect Master Password")
        elif ans == "4":
            print("Goodbye")
            ans = None
        else:
            print("Not Valid Choice Try again")
